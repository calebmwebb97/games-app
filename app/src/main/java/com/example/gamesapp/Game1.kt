package com.example.gamesapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TableLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.get
import kotlinx.android.synthetic.main.activity_game1.*
import org.w3c.dom.Text

class Game1 : AppCompatActivity() {
    var redTurn = true
    //val arr = Array(6) {IntArray(7)}
    var m = Array(6) { i -> Array(7) { j -> -1} }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game1)
    }

    fun buttonClick(view: View){
        val buSelected = view as TextView
        var canSetHere= checkSpot(view, buSelected)
        if(canSetHere) {
            var redCheck = 0
            if (redTurn) {
                redCheck = 1
            } else {
                redCheck = 0
            }
            when (buSelected.id) {
                R.id.bu1 -> m[0][0] = redCheck
                R.id.bu2 -> m[0][1] = redCheck
                R.id.bu3 -> m[0][2] = redCheck
                R.id.bu4 -> m[0][3] = redCheck
                R.id.bu5 -> m[0][4] = redCheck
                R.id.bu6 -> m[0][5] = redCheck
                R.id.bu7 -> m[0][6] = redCheck
                R.id.bu8 -> m[1][0] = redCheck
                R.id.bu9 -> m[1][1] = redCheck
                R.id.bu10 -> m[1][2] = redCheck
                R.id.bu11 -> m[1][3] = redCheck
                R.id.bu12 -> m[1][4] = redCheck
                R.id.bu13 -> m[1][5] = redCheck
                R.id.bu14 -> m[1][6] = redCheck
                R.id.bu15 -> m[2][0] = redCheck
                R.id.bu16 -> m[2][1] = redCheck
                R.id.bu17 -> m[2][2] = redCheck
                R.id.bu18 -> m[2][3] = redCheck
                R.id.bu19 -> m[2][4] = redCheck
                R.id.bu20 -> m[2][5] = redCheck
                R.id.bu21 -> m[2][6] = redCheck
                R.id.bu22 -> m[3][0] = redCheck
                R.id.bu23 -> m[3][1] = redCheck
                R.id.bu24 -> m[3][2] = redCheck
                R.id.bu25 -> m[3][3] = redCheck
                R.id.bu26 -> m[3][4] = redCheck
                R.id.bu27 -> m[3][5] = redCheck
                R.id.bu28 -> m[3][6] = redCheck
                R.id.bu29 -> m[4][0] = redCheck
                R.id.bu30 -> m[4][1] = redCheck
                R.id.bu31 -> m[4][2] = redCheck
                R.id.bu32 -> m[4][3] = redCheck
                R.id.bu33 -> m[4][4] = redCheck
                R.id.bu34 -> m[4][5] = redCheck
                R.id.bu35 -> m[4][6] = redCheck
                R.id.bu36 -> m[5][0] = redCheck
                R.id.bu37 -> m[5][1] = redCheck
                R.id.bu38 -> m[5][2] = redCheck
                R.id.bu39 -> m[5][3] = redCheck
                R.id.bu40 -> m[5][4] = redCheck
                R.id.bu41 -> m[5][5] = redCheck
                R.id.bu42 -> m[5][6] = redCheck
            }

            if (redTurn) {
                view.isClickable = false
                view.setBackgroundColor(Color.RED)
                redTurn = false
                Turn.text = "Black"
                checkWin(view)
            } else {
                view.isClickable = false
                view.setBackgroundColor(Color.BLACK)
                redTurn = true
                Turn.text = "Red"
                checkWin(view)
            }
        }
        else{
            Toast.makeText(this, "Invalid placement", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkSpot(view: View, buSelected: TextView) :Boolean{
        var pieceBelow = -1
        when (buSelected.id) {
            R.id.bu1 -> pieceBelow = m[1][0]
            R.id.bu2 -> pieceBelow = m[1][1]
            R.id.bu3 -> pieceBelow = m[1][2]
            R.id.bu4 -> pieceBelow = m[1][3]
            R.id.bu5 -> pieceBelow = m[1][4]
            R.id.bu6 -> pieceBelow = m[1][5]
            R.id.bu7 -> pieceBelow = m[1][6]
            R.id.bu8 -> pieceBelow = m[2][0]
            R.id.bu9 -> pieceBelow = m[2][1]
            R.id.bu10 -> pieceBelow = m[2][2]
            R.id.bu11 -> pieceBelow = m[2][3]
            R.id.bu12 -> pieceBelow = m[2][4]
            R.id.bu13 -> pieceBelow = m[2][5]
            R.id.bu14 -> pieceBelow = m[2][6]
            R.id.bu15 -> pieceBelow = m[3][0]
            R.id.bu16 -> pieceBelow = m[3][1]
            R.id.bu17 -> pieceBelow = m[3][2]
            R.id.bu18 -> pieceBelow = m[3][3]
            R.id.bu19 -> pieceBelow = m[3][4]
            R.id.bu20 -> pieceBelow = m[3][5]
            R.id.bu21 -> pieceBelow = m[3][6]
            R.id.bu22 -> pieceBelow = m[4][0]
            R.id.bu23 -> pieceBelow = m[4][1]
            R.id.bu24 -> pieceBelow = m[4][2]
            R.id.bu25 -> pieceBelow = m[4][3]
            R.id.bu26 -> pieceBelow = m[4][4]
            R.id.bu27 -> pieceBelow = m[4][5]
            R.id.bu28 -> pieceBelow = m[4][6]
            R.id.bu29 -> pieceBelow = m[5][0]
            R.id.bu30 -> pieceBelow = m[5][1]
            R.id.bu31 -> pieceBelow = m[5][2]
            R.id.bu32 -> pieceBelow = m[5][3]
            R.id.bu33 -> pieceBelow = m[5][4]
            R.id.bu34 -> pieceBelow = m[5][5]
            R.id.bu35 -> pieceBelow = m[5][6]
            R.id.bu36 -> pieceBelow = 2
            R.id.bu37 -> pieceBelow = 2
            R.id.bu38 -> pieceBelow = 2
            R.id.bu39 -> pieceBelow = 2
            R.id.bu40 -> pieceBelow = 2
            R.id.bu41 -> pieceBelow = 2
            R.id.bu42 -> pieceBelow = 2
        }
        return pieceBelow!=-1
    }

    private fun checkWin(view: View){
        val buSelected = view as TextView
        when (buSelected.id) {
            R.id.bu1 -> checkWinK(0,0)
            R.id.bu2 -> checkWinK(0,1)
            R.id.bu3 -> checkWinK(0,2)
            R.id.bu4 -> checkWinK(0,3)
            R.id.bu5 -> checkWinK(0,4)
            R.id.bu6 -> checkWinK(0,5)
            R.id.bu7 -> checkWinK(0,6)
            R.id.bu8 -> checkWinK(1,0)
            R.id.bu9 -> checkWinK(1,1)
            R.id.bu10 -> checkWinK(1,2)
            R.id.bu11 -> checkWinK(1,3)
            R.id.bu12 -> checkWinK(1,4)
            R.id.bu13 -> checkWinK(1,5)
            R.id.bu14 -> checkWinK(1,6)
            R.id.bu15 -> checkWinK(2,0)
            R.id.bu16 -> checkWinK(2,1)
            R.id.bu17 -> checkWinK(2,2)
            R.id.bu18 -> checkWinK(2,3)
            R.id.bu19 -> checkWinK(2,4)
            R.id.bu20 -> checkWinK(2,5)
            R.id.bu21 -> checkWinK(2,6)
            R.id.bu22 -> checkWinK(3,0)
            R.id.bu23 -> checkWinK(3,1)
            R.id.bu24 -> checkWinK(3,2)
            R.id.bu25 -> checkWinK(3,3)
            R.id.bu26 -> checkWinK(3,4)
            R.id.bu27 -> checkWinK(3,5)
            R.id.bu28 -> checkWinK(3,6)
            R.id.bu29 -> checkWinK(4,0)
            R.id.bu30 -> checkWinK(4,1)
            R.id.bu31 -> checkWinK(4,2)
            R.id.bu32 -> checkWinK(4,3)
            R.id.bu33 -> checkWinK(4,4)
            R.id.bu34 -> checkWinK(4,5)
            R.id.bu35 -> checkWinK(4,6)
            R.id.bu36 -> checkWinK(5,0)
            R.id.bu37 -> checkWinK(5,1)
            R.id.bu38 -> checkWinK(5,2)
            R.id.bu39 -> checkWinK(5,3)
            R.id.bu40 -> checkWinK(5,4)
            R.id.bu41 -> checkWinK(5,5)
            R.id.bu42 -> checkWinK(5,6)
        }
    }

    private fun checkWinK(startR: Int, startC: Int){
        //vertical check
        val fvert = checkWinV(startR, startC)

        //horizontal check
        var hori = false
        var hori2 = false
        var hori3 = false
        var hori4 = false

        //Check if the clicked space is the rightmost piece of a row, the 2nd to the right, etc.
        hori = checkWinH(startR, startC)
        if(startC-1 >= 0){
            hori2 = checkWinH(startR, startC-1)
        }
        if(startC-2 >= 0){
            hori3 = checkWinH(startR, startC-2)
        }
        if(startC-3 >= 0){
            hori4 = checkWinH(startR, startC-3)
        }

        val fhori = hori||hori2||hori3||hori4

        //Diagonal down and right
        var diag = false
        var diag2 = false
        var diag3 = false
        var diag4 = false

        diag = checkWinD(startR, startC)
        if(startR-1 >= 0 && startC-1 >= 0){
            diag2 = checkWinD(startR-1, startC-1)
        }
        if(startR-2 >= 0 && startC-2 >= 0){
            diag3 = checkWinD(startR-2, startC-2)
        }
        if(startR-3 >= 0 && startC-3 >= 0){
            diag4 = checkWinD(startR-3, startC-3)
        }
        val fdiag = diag||diag2||diag3||diag4

        //diagonal down and left
        var diagL = false
        var diagL2 = false
        var diagL3 = false
        var diagL4 = false

        diagL = checkWinDL(startR, startC)
        if(startR-1 >= 0 && startC+1 <= 6){
            diagL2 = checkWinDL(startR-1, startC+1)
        }
        if(startR-2 >= 0 && startC+2 <= 6){
            diagL3 = checkWinDL(startR-2, startC+2)
        }
        if(startR-3 >= 0 && startC+3 <= 6){
            diagL4 = checkWinDL(startR-3, startC+3)
        }
        val fdiagL = diagL||diagL2||diagL3||diagL4

        if ((fvert || fhori || fdiag || fdiagL) && m[startR][startC]!= -1){
            if(redTurn) {
                Toast.makeText(this, "Black WON !!!", Toast.LENGTH_LONG).show()
            }
            else{
                Toast.makeText(this, "Red WON !!!", Toast.LENGTH_LONG).show()
            }
        }
        else{
            println("Not connected yet.")
        }
    }

    //Checks that the column actually fits on the board, and if the 4 spaces are the same color
    private fun checkWinV(startR: Int, startC: Int) : Boolean{
        if(startR <= 2){
            val ret = m[startR][startC] == m[startR+1][startC]
            val ret2 = m[startR+2][startC] == m[startR+3][startC]
            val ret3 = m[startR][startC] == m[startR+2][startC]
            return ret && ret2 && ret3
        }
        return false
    }

    //Checks that the row actually fits on the board, and if the 4 spaces are the same color
    private fun checkWinH(startR: Int, startC: Int) : Boolean{
        if(startC <= 3){
            val ret = m[startR][startC] == m[startR][startC+1]
            val ret2 = m[startR][startC+2] == m[startR][startC+3]
            val ret3 = m[startR][startC] == m[startR][startC+2]
            return ret && ret2 && ret3
        }
        return false
    }

    //Checks that the diagonal actually fits on the board, and if the 4 spaces are the same color
    private fun checkWinD(startR: Int, startC: Int) : Boolean{
        if(startC <= 3 && startR <= 2){
            val ret = m[startR][startC] == m[startR+1][startC+1]
            val ret2 = m[startR+2][startC+2] == m[startR+3][startC+3]
            val ret3 = m[startR][startC] == m[startR+2][startC+2]
            return ret && ret2 && ret3
        }
        return false
    }

    private fun checkWinDL(startR: Int, startC: Int) : Boolean{
        if(startC >= 3 && startR <= 2){
            val ret = m[startR][startC] == m[startR+1][startC-1]
            val ret2 = m[startR+2][startC-2] == m[startR+3][startC-3]
            val ret3 = m[startR][startC] == m[startR+2][startC-2]
            return ret && ret2 && ret3
        }
        return false
    }
}
