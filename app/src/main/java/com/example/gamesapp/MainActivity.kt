package com.example.gamesapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val gameList = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        makeGamesList()

        var myAdapter = ArrayAdapter<String>(
            this, android.R.layout.simple_list_item_1, gameList
        )

        MainList.adapter = myAdapter

        MainList.setOnItemClickListener{ _, view, position, _ ->
            playClick(view, gameList[position])
            //println(storyList[position])
            myAdapter.notifyDataSetChanged()
        }
    }

    fun makeGamesList() {
        val input = Scanner(getResources().openRawResource(R.raw.games))
        var i = 0
        while (input.hasNextLine()) {
            val result = input.nextLine().trim()
            //println(result)
            gameList.add(result)
            i++
        }
    }

    fun playClick(view: View, string: String){
        var myIntent = Intent()
        if (string == "TicTacToe") {
            myIntent = Intent(this, Game0::class.java)
        }
        if (string == "Connect4"){
            myIntent = Intent(this, Game1::class.java)
        }
        if (string == "Click Fast"){
            myIntent = Intent(this, Game2::class.java)
        }
        startActivity(myIntent)
    }
}